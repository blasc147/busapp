from .base import *

DEBUG=True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '7ekfc#nf#_5&6o$8t8%)sg=ece6=@21^av)cw0s@2^z8g6=#1_'

ALLOWED_HOSTS = ['*']
DATABASES = {
    'default': {        
        'ENGINE': 'django.contrib.gis.db.backends.postgis',       
        'NAME': 'muni',
        'USER': 'postgres',
        'PASSWORD': 'asd846*',
        'HOST': 'localhost',
        'PORT': '',
        'DEFAULT_CHARSET': 'utf-8',
    }
}
