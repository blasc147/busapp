"""muni URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include, re_path
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf import settings
from .views import dashboard
from apps.novedad.views import crearNovedad, NovedadesList, obtenerNovedad, editarNovedad, eliminarNovedad
from apps.reclamo.views import ReclamoList, ListaReclamos, mapaDestinos
from apps.parada.views import ParadasList, getNearbyParadas
from apps.ruta.views import RutaList
from apps.linea.views import LineaList, LineaDetail

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    re_path(r'^dashboard/$', view=dashboard, name='dashboard'),
    re_path('login/', auth_views.LoginView.as_view(), {'template_name': "login.html"},name='login'),
    re_path('logout/', auth_views.logout_then_login, name='logout'),
    re_path(r'^crearNovedad/', view=crearNovedad, name='crearNovedad'),
    re_path(r'^obtenerNovedad/', view=obtenerNovedad, name='obtenerNovedad'),
    re_path(r'^editarNovedad/', view=editarNovedad, name='editarNovedad'),
    re_path(r'^eliminarNovedad/', view=eliminarNovedad, name='eliminarNovedad'),
    re_path(r'^reclamosAPI/$', view=ReclamoList.as_view()),
    re_path(r'^reclamos/$', view=ListaReclamos.as_view(), name='listaReclamos'),
    re_path(r'^novedadesAPI/$', view=NovedadesList.as_view()),
    re_path(r'^mapaDestinos/', view=mapaDestinos, name='mapaDestinos'),
    re_path(r'^lineasAPI/$', view=LineaList.as_view()),
    re_path(r'^detalleLineaAPI/(?P<pk>[0-9]+)/$', view=LineaDetail.as_view()),
    re_path(r'^paradasAPI/$', view=ParadasList.as_view()),
    re_path(r'^rutasAPI/$', view=RutaList.as_view()),
    re_path(r'^getNearbyParadas/$', getNearbyParadas),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
