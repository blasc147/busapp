from django.shortcuts import render, redirect
#from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from apps.novedad.models import Novedad
from apps.linea.models import Linea

#from apps.usuarios.forms import CreateUser
#from apps.core.decorators import group_required

def Start(request):
	#print(request.user.get_all_permissions())
	return render(request,'base.html')

@login_required
def dashboard(request):
	context={}
	context['lineas'] = Linea.objects.all()	
	context['noticias'] = Novedad.objects.all().order_by('-fecha')[:20]
	return render(request, 'dashboard.html', context)