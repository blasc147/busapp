from django.db import models
import io
from django.core.files.uploadedfile import InMemoryUploadedFile

from django.contrib.auth.models import AbstractUser

AbstractUser._meta.get_field('email')._unique = True

class Usuario(AbstractUser):
	dni = models.CharField(max_length=8, null=False, blank=False)
	def __str__(self):
		return self.first_name
	def get_group(self):
		if self.is_superuser:
			return "Administrador"
		if self.groups.all():
			return self.groups.all()[0].name
		else:
			return ""
	#def save(self, *args, **kwargs):
	#	if not self.avatar:
	#		return
	#	super(Usuario, self).save()
	#	filename = self.avatar.url
	#	image = Img.open("C:/Users/RURAL/Desktop/RepositorioQuAgi/src/media/user_avatar/Tulips.jpg")
	#	image.thumbnail((100,100), Img.ANTIALIAS)
	#	image.save("C:/Users/RURAL/Desktop/RepositorioQuAgi/src/media/user_avatar/Tulips.jpg")

	

