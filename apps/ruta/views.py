from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Ruta
from .serializers import RutaSerializer
from django.contrib.gis.measure import D
from rest_framework import serializers
from rest_framework import mixins
from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend

RADIUS = 1

class RutaList(mixins.ListModelMixin,
                  generics.GenericAPIView):

    queryset = Ruta.objects.all()
    serializer_class = RutaSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('linea', 'ramal')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    
    def HowArrive(request):
        input_data = request.data.copy()

        origin_ = input_data['origin'].split(',')
        origin = Point(float(origin_[0]), float(origin_[1]), srid=4326)
        detination_ = input_data['destination'].split(',')
        destination = Point(float(detination_[0]), float(detination_[1]), srid=4326)   

        paradas = Parada.objects.filter(ubicacion__distance_lt=(origin, D(km=RADIUS)))

        if len(conductores) == 0:
            if settings.DEBUG:
                return Response({MESSAGE_RESPUESTA: CONDUCTORES_NO_DISPONIBLES})

            return Response({MESSAGE_RESPUESTA: CONDUCTORES_NO_DISPONIBLES})

        # Agrego campo distancia para poder ordenar
        paradas = paradas.annotate(distance=Distance('ultima_ubicacion', origen))
        # Ordeno por distancia ascendente
        paradas = paradas.order_by('distance')

        return Response('Ruta y paradas')