from django.contrib import admin
from django.contrib.gis import admin
from .models import Ruta

# Register your models here.
class RutaAdmin(admin.OSMGeoAdmin):
    list_display = ['id', 'linea', 'ramal']
    search_fields = ['linea', 'ramal']
    default_lon = -6547794.49
    default_lat = -3183316.26
    default_zoom = 13

    class Meta:
        model = Ruta



admin.site.register(Ruta, RutaAdmin)