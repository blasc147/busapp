from django.contrib.gis.db import models

# Create your models here.
from apps.linea.models import Linea

# Create your models here.

class Ruta(models.Model):
	linea = models.ForeignKey(Linea, on_delete=models.CASCADE,)

	ruta = models.LineStringField()

	ramal = models.CharField(max_length=60,verbose_name='ramal',help_text='Ramal de la línea',null=True, blank=False)

	def __str__(self):
    		return '%s' % (self.ramal)

	class Meta:
		verbose_name = 'ruta'
		verbose_name_plural = 'rutas'