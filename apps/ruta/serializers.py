from .models import Ruta
from rest_framework import serializers

class RutaSerializer(serializers.ModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
        
    class Meta:
        model = Ruta
        fields = ('linea',
        		'ruta',
        		'ramal' ) 