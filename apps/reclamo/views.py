from django.shortcuts import render
from django.shortcuts import render
from .models import Reclamo
from .serializers import ReclamoSerializer
from rest_framework import mixins
from rest_framework import generics
from django.utils import timezone
from django.utils.safestring import mark_safe
import json
from django.views.generic.list import ListView
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

APP_NAME = 'reclamo'

class ReclamoList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    parser_class = (FileUploadParser,)
    queryset = Reclamo.objects.all()
    serializer_class = ReclamoSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

      reclamo_serializer = ReclamoSerializer(data=request.data)

      if reclamo_serializer.is_valid():
          reclamo_serializer.save()
          return Response(reclamo_serializer.data, status=status.HTTP_201_CREATED)
      else:
          return Response(reclamo_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaReclamos(ListView):

    model = Reclamo
    template_name = 'listaReclamos.html'
    context_object_name = 'reclamos_list'

    def get_context_data(self, **kwargs):
        context = super(ListaReclamos, self).get_context_data(**kwargs)
        return context

def mapaDestinos(request):
    reclamos = Reclamo.objects.all()
    
    serializer = ReclamoSerializer(reclamos, many=True)

    context = {
        'reclamos_json': mark_safe(json.dumps(serializer.data))
    }

    return render(request, 'headMap.html', context)