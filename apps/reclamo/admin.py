from django.contrib import admin
from .models import Reclamo
# Register your models here.


class ReclamoAdmin(admin.ModelAdmin):
    list_display = ('creado_por','fecha', 'linea')
    list_filter = ('creado_por', 'linea')

admin.site.register(Reclamo,ReclamoAdmin)