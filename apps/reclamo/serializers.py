from .models import Reclamo
from rest_framework import serializers
from apps.linea.serializers import LineaSerializer

class ReclamoSerializer(serializers.ModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
    #creado_por = UserPasajeroSerializer(many=False, read_only=True)
    nombre_linea = serializers.PrimaryKeyRelatedField(read_only=True)

    def get_nombre_linea(self, obj):
        return str(obj.linea)

    class Meta:
        model = Reclamo

        fields = (
            'creado_por',
        	'fecha',
            'foto', 
            'nombre_linea',
            'descripcion')