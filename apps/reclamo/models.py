from django.db import models
from apps.core.models import TimeStampedModel
from apps.usuarios.models import Usuario
from apps.linea.models import Linea
from PIL import Image as Img

class Reclamo(TimeStampedModel):
	creado_por = models.ForeignKey(Usuario, on_delete=models.CASCADE,)

	fecha = models.DateField(verbose_name=u'Fecha')

	foto = models.ImageField(upload_to = "noticia_portada",  null = False, blank=False)

	linea= models.ForeignKey(Linea,on_delete=models.CASCADE, blank=True, null=True, related_name='linea')

	descripcion = models.CharField(max_length=130, null=False, blank=False)

	class Meta:
		verbose_name = 'reclamo'
		verbose_name_plural = 'reclamos'