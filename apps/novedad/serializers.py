from .models import Novedad
from rest_framework import serializers

class NovedadSerializer(serializers.ModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """

    class Meta:
        model = Novedad

        fields = ('id',
            'creado_por',
            'fecha',
            'titulo',
            'descripcion')