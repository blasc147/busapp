from django.db import models
from apps.core.models import TimeStampedModel
from apps.usuarios.models import Usuario
from PIL import Image as Img
from datetime import datetime
from django.utils import timezone

class Novedad(TimeStampedModel):
	creado_por = models.ForeignKey(Usuario, on_delete=models.CASCADE,)

	fecha = models.DateTimeField(default=timezone.now, verbose_name=u'Fecha')

	titulo = models.CharField(max_length=60, null=False, blank=False)

	descripcion = models.TextField(max_length= 500, null=False, blank=False)



	class Meta:
		verbose_name = 'novedad'
		verbose_name_plural = 'novedades'