from django.contrib import admin
from .models import Novedad
# Register your models here.


class NovedadAdmin(admin.ModelAdmin):
    list_display = ['creado_por','fecha']
    list_filter = ['creado_por']
    fields= ['fecha', 'titulo', 'descripcion']


    def save_model(self, request, obj, form, change):             
        if not change: 
        # can use this condition also to set 'created_by'    
        # if not getattr(obj, 'created_by', None):            
            obj.creado_por = request.user
        obj.save()

admin.site.register(Novedad,NovedadAdmin)