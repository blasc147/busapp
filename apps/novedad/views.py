from django.shortcuts import render
from .models import Novedad
from .serializers import NovedadSerializer
from datetime import timedelta, datetime
from rest_framework import mixins
from rest_framework import generics
from apps.linea.models import Linea
from apps.linea.serializers import LineaSerializer
from django.utils.safestring import mark_safe
import json
from django.http import HttpResponse, Http404
# Create your views here.
def crearNovedad(request):
    if request.is_ajax():
        titulo = request.GET.get('titulo',None)
        descripcion = request.GET.get('descripcion',None)

        fecha = datetime.now()
        try:

            nuevaNovedad = Novedad.objects.create(
                                        creado_por=request.user,
                                        titulo=titulo,
                                        descripcion=descripcion,
                                        fecha= fecha,
                                        )
        except Exception as e:
            l.exception(u'%s; %s', e, str(e))
            raise Http404 
        

        serializer = NovedadSerializer(nuevaNovedad)         
        

        data = json.dumps(serializer.data)
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404 

class NovedadesList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = Novedad.objects.all()
    serializer_class = NovedadSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

def obtenerNovedad(request):
    if request.is_ajax():
        ide = request.GET.get('id',None)

        fecha = datetime.now()
        novedad = Novedad.objects.get(pk = ide)
        context={}

        context['novedad'] = NovedadSerializer(novedad).data
        
        data = mark_safe(json.dumps(context))

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404 

def editarNovedad(request):
    if request.is_ajax():
        ide = request.GET.get('id',None)
        titulo = request.GET.get('titulo',None)
        descripcion = request.GET.get('descripcion',None)
        novedad = Novedad.objects.get(pk = ide)

        novedad.titulo=titulo
        novedad.descripcion=descripcion

        novedad.save()
        context={}

        context['novedad'] = NovedadSerializer(novedad).data
        
        data = mark_safe(json.dumps(context))

        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404         

def eliminarNovedad(request):
    if request.is_ajax():
        ide = request.GET.get('id',None)

        novedad = Novedad.objects.get(pk = ide)

        novedad.delete()
               
        
        data = json.dumps('ok')
        mimetype = 'application/json'
        return HttpResponse(data, mimetype)
    else:
        raise Http404      