# Generated by Django 2.0 on 2018-10-10 13:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('linea', '0002_auto_20181010_1059'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Novedad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creado', models.DateTimeField(auto_now_add=True, help_text='Fecha de creación', verbose_name='creado')),
                ('modificado', models.DateTimeField(auto_now=True, help_text='Fecha de modificación', verbose_name='modificado')),
                ('fecha', models.DateField(verbose_name='Fecha')),
                ('titulo', models.CharField(max_length=60)),
                ('descripcion', models.CharField(max_length=150)),
                ('creado_por', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('linea', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='linea.Linea')),
            ],
            options={
                'verbose_name_plural': 'novedades',
                'verbose_name': 'novedad',
            },
        ),
    ]
