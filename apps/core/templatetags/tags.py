from django import template
register = template.Library()


@register.filter
def notNone(string):
	if string == None:
		return '-'
	else:
		return string