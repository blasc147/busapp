# -*- coding: utf-8 -*-

from django.db import models


class TimeStampedModel(models.Model):
    """An abstract base class model that provides selfupdating
    ``created`` and ``modified`` fields."""
    creado = models.DateTimeField(auto_now_add=True,
                                  verbose_name=u'creado',
                                  help_text=u'Fecha de creación')
    modificado = models.DateTimeField(auto_now=True,
                                      verbose_name=u'modificado',
                                      help_text=u'Fecha de modificación')

    class Meta:
        abstract = True