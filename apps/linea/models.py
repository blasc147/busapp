from django.db import models


# Create your models here.

class Linea(models.Model):
	numero = models.CharField(max_length=10,verbose_name='nombre',help_text='Numero de linea')

	empresa = models.CharField(max_length=60,verbose_name='empresa',help_text='Nombre de la empresa')

	def __str__(self):
		return 'linea  %s' % (self.numero)

	class Meta:
		verbose_name = 'linea'
		verbose_name_plural = 'lineas'