from .models import Linea
from rest_framework import serializers

class LineaSerializer(serializers.ModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
        
    class Meta:
        model = Linea
        fields = ('id','numero',
        		'empresa' )
        depth = 1
