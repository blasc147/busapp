from django.contrib import admin
from .models import Linea
# Register your models here.


class LineaAdmin(admin.ModelAdmin):
    list_display = ('numero','empresa')

admin.site.register(Linea,LineaAdmin)