from django.shortcuts import render
from .serializers import LineaSerializer
from .models import Linea
from rest_framework import generics

# Create your views here.
class LineaDetail(generics.DestroyAPIView,
                      generics.UpdateAPIView,
                      generics.RetrieveAPIView):
    queryset = Linea.objects.all()
    serializer_class = LineaSerializer


class LineaList(generics.ListAPIView):
    queryset = Linea.objects.all()
    serializer_class = LineaSerializer