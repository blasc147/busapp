from django.contrib.gis.db import models
from apps.linea.models import Linea
from apps.ruta.models import Ruta

# Create your models here.

class Parada(models.Model):
	linea = models.ForeignKey(Linea, on_delete=models.CASCADE,)
	ruta = models.ForeignKey(Ruta, on_delete=models.DO_NOTHING,null=True, blank=False)
	codigo = models.CharField(max_length=60,verbose_name='codigo',help_text='Codigo de la parada')

	ubicacion = models.PointField(null=True, blank=True, default='POINT(0 0)', srid=4326)

	descripcion = models.CharField(max_length=200,verbose_name='descripción',help_text='Descripción de la parada', null=True, blank=True)

	class Meta:
		verbose_name = 'parada'
		verbose_name_plural = 'paradas'