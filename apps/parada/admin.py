from django.contrib import admin
from django.contrib.gis import admin
from .models import Parada

# Register your models here.
class ParadaAdmin(admin.OSMGeoAdmin):
    list_display = ['linea', 'ruta', 'codigo', 'descripcion']
    search_fields = ['linea', 'ruta', 'codigo', 'descripcion']
    default_lon = -6547794.49
    default_lat = -3183316.26
    default_zoom = 13

    class Meta:
        model = Parada



admin.site.register(Parada, ParadaAdmin)