from .models import Parada
from rest_framework import serializers
from rest_framework import mixins
from rest_framework import generics, permissions
from .serializers import ParadaSerializer
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

RADIUS = 800

class ParadasList(mixins.ListModelMixin,
                  generics.GenericAPIView):
    queryset = Parada.objects.all()
    serializer_class = ParadaSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


@api_view (['GET'])
def getNearbyParadas(request):
    
    input_data = request.GET

    linea_id = input_data['linea']
    origen_a = input_data['origen'].split(',')
    origen = Point(float(origen_a[1]), float(origen_a[0]), srid=4326)
    
    qty = 6
    if 'qty' in input_data:
        qty = int(input_data['qty'])

    paradas = Parada.objects.filter(linea=linea_id,
                                    ubicacion__distance_lt=(origen, D(m=RADIUS)))

    if len(paradas) > 0:
        # Agrego campo distancia para poder ordenar
        paradas = paradas.annotate(distance=Distance('ubicacion', origen))
        # Ordeno por distancia ascendente
        paradas = paradas.order_by('distance')[:qty]

        serializer = ParadaSerializer(paradas, many=True)

        return Response(serializer.data)
    
    return Response("No hay paradas cercanas")
