from .models import Parada
from rest_framework import serializers

class ParadaSerializer(serializers.ModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
        
    class Meta:
        model = Parada
        fields = ('linea',
        		'codigo',
        		'ruta',
        		'ubicacion',
        		'descripcion' )